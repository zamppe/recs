<?php

/**
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
if (! function_exists('user')) {
    function user()
    {
        if(auth()->check()) {
            return auth()->user();
        }

        return null;
    }
}

/**
 * convert milliseconds to 01:23:45,67 format or 01:23,45 format
 * @param $ms
 * @param $nDecimal
 * @return string
 */
function mstostrtime($ms, $nDecimal = 2)
{
    if ($nDecimal === 3) {
        return sprintf("%02d:%02d,%03d", $ms / 60 / 1000 % 60, $ms / 1000 % 60, $ms % 1000);
    }

    return sprintf("%02d:%02d,%02d", $ms / 60 / 1000 % 60, $ms / 1000 % 60, $ms / 10 % 100);
}
