<?php

namespace App\Recplay;

class RecplayAssetsDownloader 
{
    const BASE_URL_IMAGES = 'http://maxdamantus.github.io/recplay/images';
    const BASE_URL_PICTS = 'http://maxdamantus.github.io/recplay/images/picts';
    /**
     * https://stackoverflow.com/a/3938844
     */
    private static function downloadFile(string $url, string $path)
    {
        error_reporting(E_ERROR);
        $newfname = $path;
        $file = fopen ($url, 'rb');
        if ($file) {
            $newf = fopen ($newfname, 'wb');
            if ($newf) {
                while(!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($newf) {
            fclose($newf);
        }
    }

    /**
     * 
     */
    public static function downloadAndSaveAll()
    {
        $imageNames = [
            "bike.png", 
            "ground.png", 
            "head.png", 
            "sky.png", 
            "susp1.png", 
            "susp2.png", 
            "wheel.png", 
            "qfood1.png", 
            "qfood2.png", 
            "qkiller.png", 
            "qexit.png", 
            "q1body.png", 
            "q1forarm.png", 
            "q1leg.png", 
            "q1thigh.png", 
            "q1up_arm.png", 
            "myshirt.png",
        ];

        $pictNames = [
            "qgrass.png",
            "qdown_1.png",
            "qdown_14.png",
            "qdown_5.png",
            "qdown_9.png",
            "qup_0.png",
            "qup_1.png",
            "qup_14.png",
            "qup_5.png",
            "qup_9.png",
            "qup_18.png",
            "qdown_18.png",
            "cliff.png",
            "stone1.png",
            "stone2.png",
            "stone3.png",
            "st3top.png",
            "brick.png",
            "qfood1.png",
            "qfood2.png",
            "bridge.png",
            "sky.png",
            "tree2.png",
            "bush3.png",
            "tree4.png",
            "tree5.png",
            "log2.png",
            "sedge.png",
            "tree3.png",
            "plantain.png",
            "bush1.png",
            "bush2.png",
            "ground.png",
            "flag.png",
            "secret.png",
            "hang.png",
            "edge.png",
            "mushroom.png",
            "log1.png",
            "tree1.png",
            "maskbig.png",
            "maskhor.png",
            "masklitt.png",
            "barrel.png",
            "supphred.png",
            "suppvred.png",
            "support2.png",
            "support3.png",
            "support1.png",
            "suspdown.png",
            "suspup.png",
            "susp.png",
        ];

        $saveToPath = storage_path('app');

        foreach ($imageNames as $imageName) {
            self::downloadFile(implode('/', [
                self::BASE_URL_IMAGES, 
                $imageName,
            ]), implode(DIRECTORY_SEPARATOR, [
                $saveToPath,
                $imageName,
            ]));
        }

        \Storage::makeDirectory('picts');

        foreach ($pictNames as $pictName) {
            self::downloadFile(implode('/', [
                self::BASE_URL_PICTS, 
                $pictName,
            ]), implode(DIRECTORY_SEPARATOR, [
                $saveToPath,
                'picts',
                $pictName,
            ]));
        }
    }
}
