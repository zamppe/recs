<?php

namespace App\Http\Controllers;

use App\Replay;
use Illuminate\Http\Request;

class ReplayController extends Controller
{
    /**
     * Show the replay
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $replayId)
    {
        $replay = Replay::find($replayId);

        $replaysQuery = \App\Replay::orderBy('replays.id', 'desc')
            ->select('replays.*')
            ->with('user');

        $replays = \App\Services\Replay::filterByRequest($request, $replaysQuery);

        if ($request->wantsJson()) {
            $recplayContainerRendered = view()->make('partials/recplay-container', compact('replay'))->render();
            $replayListRendered = view()->make('partials/replay-list', compact('replay', 'replays'))->render();
            $replayHeaderRendered = view()->make('partials/replay-header', compact('replay'))->render();

            return response()->json([
                'replay_list' => $replayListRendered,
                'recplay_container' => $recplayContainerRendered,
                'replay_header' => $replayHeaderRendered,
                'rec_name' => $replay->rec_name,
                'rec_url' => $replay->recurl,
                'lev_url' => $replay->levurl,
                'app_title' => implode(' - ', [
                    config('app.name', 'Laravel'),
                    $replay->rec_name
                ]),
            ]);
        }

        return view('replays.show', compact('replay', 'replays'));
    }

    public function download($replayId)
    {
        $replay = Replay::find($replayId);

        return response()->download($replay->firstMedia('rec')->getAbsolutePath());
    }
}
