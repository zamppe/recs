<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\LevelDownloader;
use App\Replay;
use App\Rec\Rec;
use Illuminate\Http\Request;
use MediaUploader;

class ReplayController extends Controller
{
    /**
     * Show the rec upload form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('user.replays.create');
    }

    /**
     * Store a replay
     *
     * @param Replay $replay
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Replay $replay, Request $request)
    {
        $request->validate([
            'file' => 'required',
            'legit' => 'boolean',
            'description' => 'string|nullable',
        ]);

        if ($request->file('file')) {
            /* store the file into the filesystem and create media */
            $recMedia = MediaUploader::fromSource($request->file('file'))
                ->upload();

            $levName = Rec::extractLevName($recMedia->getAbsolutePath());

            $request->validate([
                'file' => function($attribute, $value, $fail) use($request) {
                    $fileName = pathinfo($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME);
                    $replayWithSameName = Replay::where('rec_name', $fileName)->first();

                    if ($replayWithSameName) {
                        return $fail("Rec with name {$fileName} already exists");
                    }
                },
            ]);

            $crc = Rec::extractCRC($recMedia->getAbsolutePath());

            if (($downloader = LevelDownloader::findMatchingLevel($levName, $crc))->getStatus()) {
                $levMedia = MediaUploader::importPath('public', implode(DIRECTORY_SEPARATOR, [
                    $downloader->getTempDirectory(),
                    $levName,
                ]));
            } else {
                return redirect()->route('u.replays.create');
            }

            $recInfo = Rec::recInfo($recMedia->getAbsolutePath());

            $replay = user()->replays()->create([
                'lev_name' => $levMedia->filename,
                'rec_name' => $recMedia->filename,
                'legit' => $request->input('legit'),
                'description' => $request->input('description'),
                'finished' => $recInfo->finished,
                'duration' => $recInfo->duration,
            ]);

            /* attach the media into the Replay model */
            $replay->attachMedia($recMedia, 'rec');
            $replay->attachMedia($levMedia, 'lev');

            $recMedia->move(implode(DIRECTORY_SEPARATOR, [
                'replays',
                $replay->id,
                'rec'
            ]));

            $levMedia->move(implode(DIRECTORY_SEPARATOR, [
                'replays',
                $replay->id,
                'rec'
            ]));

            return redirect()->route('replays.show', [
                $replay,
                $replay->rec_name,
            ]);
        }
    }

    public function edit(int $replayId)
    {
        $replay = Replay::find($replayId);

        if (! $replay->userHasPermission()) {
            return back();
        }

        return view('user.replays.edit', compact('replay'));
    }

    public function update(int $replayId, Request $request)
    {
        $replay = Replay::find($replayId);

        if (! $replay->userHasPermission()) {
            return back();
        }

        $validatedData = $request->validate([
            'legit' => 'boolean',
            'description' => 'string|nullable',
        ]);

        $replay->update($validatedData);

        return redirect()->route('replays.show', [
            $replay,
            $replay->rec_name,
        ]);
    }
}
