<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the home page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $replaysQuery = \App\Replay::orderBy('replays.id', 'desc')
            ->select('replays.*')
            ->with('user');

        $replays = \App\Services\Replay::filterByRequest($request, $replaysQuery);

        if ($request->wantsJson()) {
            $replayListRendered = view()->make('partials/replay-list', compact('replays'))->render();

            return response()->json(['replay_list' => $replayListRendered]);
        }

        return view('welcome', compact('replays'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function dashboard()
    {
        return redirect('/');
    }
}
