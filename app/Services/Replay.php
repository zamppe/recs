<?php

namespace App\Services;

class Replay
{
    /**
     * Filter a replay model query builder object based on request parameters
     *
     * @param type $request
     * @param type $replaysQuery
     * @return \Illuminate\Support\Collection
     */
    public static function filterByRequest($request, $replaysQuery)
    {
        if ($levelName = $request->input('level')) {
            $replaysQuery->levelLike($levelName);
        }

        if ($uploaderName = $request->input('uploader')) {
            $replaysQuery->join('users', 'replays.user_id', '=', 'users.id')
                ->where(function($q) use($uploaderName) {
                    $q->where('users.name', 'like', "%{$uploaderName}%")
                      ->orWhere('users.email', 'like', "%{$uploaderName}%");
                });
        }

        if ($type = $request->input('type')) {
            if ((int) $type === 1) {
                $replaysQuery->legit();
            } else if ((int) $type === 2) {
                $replaysQuery->tas();
            }
        }

        return $replaysQuery->get();
    }
}
