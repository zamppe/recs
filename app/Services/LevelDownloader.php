<?php

namespace App\Services;

use DOMDocument;
use File as FileFacade;

class LevelDownloader
{
    protected $tempDirectory;
    protected $tempPath;
    protected $absoluteTempPath;
    protected $crcInRec;
    protected $levName;
    protected $status;

    public function __construct($levName, $crcInRec)
    {
        $this->tempDirectory = "temp";

        $this->tempPath = implode(DIRECTORY_SEPARATOR, [
            "app",
            "public",
            $this->tempDirectory
        ]);

        $this->absoluteTempPath = storage_path(implode(DIRECTORY_SEPARATOR, [
            $this->tempPath,
            $levName
        ]));

        $this->crcInRec = $crcInRec;
        $this->levName = $levName;

        \Storage::disk('public')->makeDirectory($this->tempDirectory);
    }

    private function getLevelIds()
    {
        $ids = collect();
        $levName = explode('.', $this->levName)[0];
        $url = "http://elmaonline.net/ajax/search/{$levName}/level";
        $doc = new DOMDocument();
        $doc->loadHTMLFile($url);
        $rows = $doc->getElementsByTagName('a');

        if (! $rows->length) {
            return $ids;
        }

        foreach ($rows as $row) {
            $href = $row->getAttribute('href');
            $arr = explode('/', $href);
            $ids->push(end($arr));
        }

        return $ids->unique();
    }

    private function crcMatch($levPath) {
        $f = fopen($levPath, 'rb');
        fread($f, 7);
        $crcInLev = bin2hex(fread($f, 4));
        fclose($f);

        if ($crcInLev === $this->crcInRec) {
            return true;
        }

        return false;
    }

    private function findCorrectLevel()
    {
        if (in_array($this->levName, config('internals'))) {
            $intPath = storage_path(join(DIRECTORY_SEPARATOR, array("app", "internals", strtolower($this->levName))));
            FileFacade::copy($intPath, $this->absoluteTempPath);

            return true;
        } else {
            $levIds = $this->getLevelIds();
            $baseUri = "http://elmaonline.net/downloads/lev/";

            foreach ($levIds as $levId) {
                /* Download and save lev into a temp file for crc inspection */
                $resource = new \GuzzleHttp\Psr7\LazyOpenStream($this->absoluteTempPath, 'w');
                $client = new \GuzzleHttp\Client([
                    'base_uri' => $baseUri,
                ]);

                $client->request('GET', (string) $levId, ['sink' => $resource]);
                $resource->close();

                if ($this->crcMatch($this->absoluteTempPath)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getTempDirectory()
    {
        return $this->tempDirectory;
    }

    public static function findMatchingLevel($levName, $crcInRec)
    {
        $downloader = new self($levName, $crcInRec);
        $downloader->status = $downloader->findCorrectLevel();

        return $downloader;
    }
}
