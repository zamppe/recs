<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;


/**
 * Class Replay
 * @package App
 * @mixin \Eloquent
 */
class Replay extends Model
{
    use Mediable;

    protected $fillable = [
        'lev_name',
        'rec_name',
        'token',
        'legit',
        'description',
        'finished',
        'duration',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope for TAS recs
     *
     * @param type $query
     * @param type $levelName
     * @return type
     */
    public function scopeTas($query)
    {
        return $query->where('legit', false);
    }

    /**
     * Scope for legit recs
     *
     * @param type $query
     * @param type $levelName
     * @return type
     */
    public function scopeLegit($query)
    {
        return $query->where('legit', true);
    }

    /**
     * Scope for filtering results with exact or approximate level filename
     *
     * @param type $query
     * @param type $levelName
     * @return type
     */
    public function scopeLevelLike($query, $levelName)
    {
        return $query->where('lev_name', 'like', "%$levelName%");
    }

    /**
     * Accessor for getting created at attribute
     *
     * @param $value
     * @return Carbon
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value);
    }

    /**
     * Accessor for getting lev url attribute
     *
     * @return string
     */
    public function getLevUrlAttribute()
    {
        return $this->firstMedia('lev')->getUrl();
    }

    /**
     * Accessor for getting rec url attribute
     *
     * @return string
     */
    public function getRecUrlAttribute()
    {
        return $this->firstMedia('rec')->getUrl();
    }


    /**
     * Check if the current user has permission to this replay
     *
     * @return bool
     */
    public function userHasPermission()
    {
        return auth()->user() && (
                auth()->user()->id === $this->user->id ||
                auth()->user()->id === 1);
    }
}
