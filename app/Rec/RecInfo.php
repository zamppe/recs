<?php namespace App\Rec;

class RecInfo {
    public $finished;
    public $duration;

    public function __construct($finished, $duration) {
        $this->finished = $finished;
        $this->duration = $duration;
    }
}
