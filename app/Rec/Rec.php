<?php namespace App\Rec;

class Rec {
    /**
     * Extract the level name from rec binary
     *
     * @param string $recPath
     * @return string
     */
    public static function extractLevName($recPath)
    {
        $f = fopen($recPath, 'rb');
        fread($f, 20);
        $levName = trim(fread($f, 12), "\t\n\r\0\x0B");
        $nullTerminator = strpos($levName, "\x00");

        if ($nullTerminator !== false) {
            $levName = substr($levName, 0, $nullTerminator);
        }

        fclose($f);

        return $levName;
    }

    /**
     * Extract CRC from rec binary
     *
     * @param string $recPath
     * @return string
     */
    public static function extractCRC($recPath)
    {
        $f = fopen($recPath, 'rb');
        fread($f, 16);
        $crc = bin2hex(fread($f, 4));

        fclose($f);

        return $crc;
    }

    /**
     * Get info about rec
     *
     * @param string $recPath
     * @return RecInfo
     */
    public static function recInfo($recPath)
    {
        $f = fopen($recPath, 'rb');

        $nFrames = unpack("V", fread($f, 4))[1];
        fread($f, 32);
        fread($f, 27*$nFrames);
        $nEvents = unpack("V", fread($f, 4))[1];

        $finishTimeMs = 0;
        for ($eventIndex = 0; $eventIndex < $nEvents; $eventIndex++) {
            $eventTime = unpack("e", fread($f, 8))[1];
            $eventInfo = unpack("v", fread($f, 2))[1];
            $eventType = unpack("C", fread($f, 1))[1];
            fread($f, 5);

            if ($eventType === 0) {
                $finishTimeMs = (int) ($eventTime * (62500 / 273) * 10);
            }
        }

        $framesDurationMs = (int) ($nFrames / 30 * 1000);

        fclose($f);

        $finished = $framesDurationMs - $finishTimeMs <= 40;
        $duration = $finished ? $finishTimeMs : $framesDurationMs;

        return new RecInfo($finished, $duration);
    }
}
