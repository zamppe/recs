let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .copy('resources/assets/js/vendor/recplay', 'public/recplay')
  .copy('resources/assets/js/vendor/amd.js/amd.js', 'public/js')
  .copy('resources/assets/js/scripts.js', 'public/js')
  .copy('resources/assets/fonts', 'public/fonts')
  .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/', 'public/fonts')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .js('resources/assets/js/app.js', 'public/js')
  .version()
  .options({
    processCssUrls: false
  });
