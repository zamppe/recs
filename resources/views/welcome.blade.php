@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row replay js-replay">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="js-replay-header">
                    </div>

                    <div class="js-recplay">
                        <div class="panel-body rec p-4" id="recplay-container">
                            <div id="recplay"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row px-4 pb-4">
            <div class="col-md-10 col-md-offset-1">
                <form action="{{ route('home')}}" method="GET" class="js-replay-filter-form">
                    @include('partials.replay-filter-form')
                </form>
            </div>
        </div>
    </div>

    <div class="js-replay-list-container">
        @include('partials/replay-list')
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/amd.js') }}"></script>
@endsection
