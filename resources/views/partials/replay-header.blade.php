<div class="panel-heading p-4"><h1 class="replay-title">{{ $replay->rec_name }} @unless ($replay->legit)
    <span class="legit tas-flag--large pull-right">TAS</span> @endunless</h1>

    <div class="replay-header">
        <div>
            @if ($replay->description)
                <p class="replay-header__description">{{ $replay->description }}</p>
            @endif

            <p class="m-0">
                Uploaded by: {{ $replay->user->name }} <br> {{ $replay->created_at }} <br>
            </p>

            <a href="{{ route('replays.download', $replay) }}"><span class="glyphicon glyphicon-download-alt"></span> Download rec</a>
        </div>

        <div class="replay-header__right">
            @if ($replay->userHasPermission())
                <a class="btn btn-primary" href="{{ route('u.replays.edit', $replay) }}">Edit</a>
            @endif

            <p style="margin: 0; text-align: end">Duration: <b class="numeric">
                    {{ $replay->finished ?
                    mstostrtime($replay->duration, 3) :
                    mstostrtime($replay->duration, 2) }}
                </b>
            </p>
        </div>
    </div>
</div>