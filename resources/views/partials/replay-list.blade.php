<div class="container">
    <div class="row px-4 pb-5 mb-5">
        <div class="col-md-10 col-md-offset-1">
            <ul class="replay-list m-0 p-0 js-replay-list">
                @foreach ($replays as $replayItem)
                    <li class="replay-list__item">
                        <a class="js-load-replay replay-list__link{{ (isset($replay) && $replayItem->id === $replay->id) ? ' replay-list__link--active' : ''}}"
                           href="{{ route('replays.show', [
                                $replayItem,
                                'rec_name' => $replayItem->rec_name,
                                'level' => request()->input('level'),
                                'uploader' => request()->input('uploader'),
                                'type' => request()->input('type'),
                            ]) }}">
                            <div class="replay-list__recname">
                                {{ str_limit($replayItem->rec_name, 16) }}
                            </div>

                            <div class="replay-list__levname">
                                {{ $replayItem->lev_name }}
                            </div>

                            <div class="replay-list__username">
                                {{ $replayItem->user->name }}
                            </div>

                            <div class="replay-list__time">
                                @if ($replayItem->finished)
                                    <span class="numeric">
                                        {{ mstostrtime($replayItem->duration, 2) }}
                                    </span>
                                @else
                                    <span>DNF</span>
                                @endif
                            </div>

                            <div class="replay-list__createdat">
                                {{ $replayItem->created_at->diffForHumans() }}
                            </div>

                            <div class="replay-list__legit pr-2">
                                @unless ($replayItem->legit) <span class="legit">TAS</span> @endunless
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
