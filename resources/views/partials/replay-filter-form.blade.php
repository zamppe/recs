{{ csrf_field() }}

<div class="form-group">
    <label class="mr-1" for="level">
        Level
        <input type="text" id="level" name="level" class="form-control" value={{ request()->input('level') }}>
    </label>

    <label class="mr-1" for="uploader">
        Uploader
        <input type="text" id="uploader" name="uploader" class="form-control" value="{{ request()->input('uploader') }}">
    </label>

    <label class="mr-1" for="type">
        Type
        <select name="type" id="type" class="form-control">
            <option {{ (int) request()->input('type') !== 1 || (int) request()->input('type') !== 2 ? 'selected' : ''}}></option>
            <option value="1" {{ (int) request()->input('type') === 1 ? 'selected' : ''}}>Legit</option>
            <option value="2" {{ (int) request()->input('type') === 2 ? 'selected' : ''}}>TAS</option>
        </select>
    </label>
    <button type="submit" class="btn btn-primary mr-1" style="margin-bottom: 2px">Submit</button>
    <button type="button" class="btn btn-secondary js-replay-filter-form-clear" style="margin-bottom: 2px">Clear</button>
</div>
