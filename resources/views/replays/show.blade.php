@extends('layouts.app')

@section('title')
    {{ config('app.name', 'Laravel') }} - {{ $replay->rec_name }}
@endsection

@section('content')
    <div class="container">
        <div class="row replay js-replay">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="js-replay-header">
                        @include('partials.replay-header')
                    </div>

                    <div class="js-recplay">
                        @include('partials.recplay-container')
                    </div>
                </div>
            </div>
        </div>

        <div class="row px-4 pb-4">
            <div class="col-md-10 col-md-offset-1">
                <form action="{{ route('replays.show', [$replay, $replay->rec_name]) }}" class="js-replay-filter-form"
                      method="GET">
                    @include('partials.replay-filter-form')
                </form>
            </div>
        </div>
    </div>

    <div class="js-replay-list-container">
        @include('partials/replay-list')
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('js/amd.js') }}"></script>
    <script>
      window.onload = function() {
        reinitRecplay();
      };
    </script>
@endsection
