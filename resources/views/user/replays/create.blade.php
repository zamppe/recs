@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Upload</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('u.replays.store') }}" enctype="multipart/form-data"> 
                        {{ csrf_field() }}                       
                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="file" class="col-md-4 control-label">File</label>

                            <div class="col-md-6">
                                <input id="file" type="file" class="form-control" name="file" value="{{ old('file') }}" required autofocus>

                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 text-right">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" id="description" cols="40" rows="5">{{ old('description', '') }}</textarea>
                            </div>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                            <label for="legit" class="col-md-4 text-right">TAS</label>
                            <div class="col-md-6">
                                <input type="hidden" value="1" name="legit">
                                <input type="checkbox" id="legit" name="legit" value="{{ old('legit', 0)  }}" class="checkbox-inline">
                            </div>

                            @if ($errors->has('legit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('legit') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
