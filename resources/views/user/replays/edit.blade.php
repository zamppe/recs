@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('u.replays.update', $replay) }}">
                        <input name="_method" type="hidden" value="PUT">

                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 text-right">Description</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="description" id="description" cols="40" rows="5">{{ $replay->description }}</textarea>
                            </div>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('legit') ? ' has-error' : '' }}">
                            <label for="legit" class="col-md-4 text-right">TAS</label>
                            <div class="col-md-6">
                                <input type="hidden" value="1" name="legit">
                                <input class="checkbox-inline" type="checkbox" id="legit" name="legit" value="{{ old('legit', 0)  }}" {{ $replay->legit === 0 ? 'checked' : '' }}>
                            </div>

                            @if ($errors->has('legit'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('legit') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
