window.appState = {
  recplayController: undefined,
  isPlaying: false
};

var getRecplayContainerDimensions = function() {
  var container = document.getElementById('recplay-container');

  if (! container) {
    return {
      w: 0,
      h: 0
    };
  }

  return {
    w: container.offsetWidth - 32,
    h: container.offsetHeight - 32,
  };
};

var reinitRecplay = function(recUrl, levUrl) {
  if (window.appState.recplayController !== undefined) {
    window.appState.recplayController.loadLevel(levUrl);
    setTimeout(function() {
      window.appState.recplayController.loadReplay(recUrl);
    }, 50);
    updateIsPlaying(true);
  } else {
    require(["../../recplay/controller"], function(controller){
      controller(
        recplay.dataset.levurl ? recplay.dataset.levurl : levUrl,
        '/recplay/images',
        document.getElementById("recplay"),
        document
      )(function(recplayController){
        updateIsPlaying(true);
        recplayController.loadReplay(recplay.dataset.recurl ? recplay.dataset.recurl :  recUrl);
        resizeRecplayCanvas(recplayController, getRecplayContainerDimensions());
        window.appState.recplayController = recplayController;
      });
    });
  }
};

var resizeRecplayCanvas = function(recplayController, dimensions) {
  recplayController.resize(dimensions.w, dimensions.h);
};

var syncPlayingStateToHtml = function() {
  if (isPlaying()) {
    $('.js-replay')[0].dataset.playing = '';
  } else {
    $('.js-replay')[0].removeAttribute('data-playing');
  }
};

var updateIsPlaying = function(isPlaying) {
  window.appState.isPlaying = isPlaying;
  syncPlayingStateToHtml();
};

var isPlaying = function() {
  return window.appState.isPlaying;
};

var stopPlaying = function() {
  if (window.appState.recplayController === undefined || ! isPlaying()) {
    return;
  }

  updateIsPlaying(false);
};

var loadReplay = function(url, pushState) {
  var scrollPosition = $('.replay-list')[0].scrollTop;

  $.get({
    url: url,
    dataType: 'json',
  }).then(function(response) {
    reinitRecplay(response['rec_url'], response['lev_url']);
    $('.js-replay-list-container').html(response['replay_list']);
    $('.js-replay-list')[0].scrollTop = scrollPosition;
    $('.js-replay-header').html(response['replay_header']);
    $('.js-title').html(response['app_title']);

    if (pushState) {
      window.history.pushState(null, url, url);
    }
  });
};

var refreshReplayList = function() {
  var $form = $('.js-replay-filter-form');

  $.get({
    url: $form.attr('action'),
    dataType: 'json',
    data: $form.serialize()
  }).then(function(response) {
    $('.js-replay-list-container').html(response['replay_list']);
  });
};

$(document).on('submit', '.js-replay-filter-form', function(e) {
  e.preventDefault();

  refreshReplayList();
});

$(document).on('click', '.js-replay-filter-form-clear', function(e) {
  e.preventDefault();

  $filterForm = $('.js-replay-filter-form');
  $filterForm.find("input[type=text]").val('');
  $filterForm.find('select').val(0).prop('selected', true);

  refreshReplayList();
});

$(document).on('click', '.js-load-replay', function(e) {
  e.preventDefault();

  loadReplay($(this).attr('href'), true);
});

$(window).on('resize', function() {
  if (window.appState.recplayController === undefined) {
    return;
  }

  resizeRecplayCanvas(window.appState.recplayController, getRecplayContainerDimensions());
});

window.onpopstate = function(e) {
  if (window.location.pathname === '/') {
    stopPlaying();
  } else {
    loadReplay(window.location.href, false);
  }
};

window.history.pushState(null, window.location.href, window.location.href);
