<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@dashboard')->name('dashboard');
Route::get('replays/{replay}/show', 'ReplayController@show')->name('replays.show');
Route::get('replays/{replay}/download', 'ReplayController@download')->name('replays.download');

Route::group([
    'prefix' => 'u',
    'as' => 'u.',
    'namespace' => 'User',
    'middleware' => ['auth']
], function () {
    Route::get('replays/create', 'ReplayController@create')->name('replays.create');
    Route::post('replays/store', 'ReplayController@store')->name('replays.store');
    Route::get('replays/{replay}/edit', 'ReplayController@edit')->name('replays.edit');
    Route::put('replays/{replay}/update', 'ReplayController@update')->name('replays.update');
});
